"""
Build support for augercmake extra modules for cmake_
-----------------------------------------------------
"""
import os
import os.path

from ApeTools import Config, Build


class AugerCMake(Build.Package):

    def isInstalled(self):
        """Special function to check if the package is installed.
        This is needed because of the non-standard installation path needed
        to make sure cmake_ can locate the extra modules.
        """
        return os.path.exists(os.path.join(Config.get("ape", "base"), "share",
                                           "auger", "cmake",
                                           "AugerCMakeConfig.cmake"))
