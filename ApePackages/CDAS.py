"""
Build support for the cdas_ package
-----------------------------------
"""
import os
import os.path
from ApeTools import Config, Build
from ApeTools.Build import InstallError


class CDAS(Build.Package):
    """Customizations for the cdas_ package.

    The cdas_ package is built in place. Is also provides a large number of
    environment variables which can cannot be set statically.
    """

    builder_default = "cmake"

    def __init__(self, name):
        """Set attribute :attr:`cmtSystem`.

        It follows the algorithm of CMT_ for normal system. The special
        case of running on a system with AFS is not take into account.
        """
        Build.Package.__init__(self, name)
        self.cmtSystem = os.uname()[0]
        if self.cmtSystem == "Linux":
            self.cmtSystem += "-" + os.uname()[4]

    def packageVersion(self, package):
        """Determine the version of one of the sub-packages (in CMT_
        nomenclature).

        Expects that there is only one directory in the package directory. Uses
        the name of this directory as the package version.

        :Example: if we have ``IoSd/v2r8``, the version of the ``IoSd`` package
                  is ``v2r8``.
        """
        d = os.listdir(os.path.join(self.prefix, package))
        if len(d) != 1:
            raise InstallError(args=["Cannot determine version "
                                     "of sub-package %s" % package],
                               pack="cdas")
        else:
            return d[0]

    def env(self):
        """Set the environment.

        Determine the location of all library packages and set the
        :envvar:`...HOME` variables as well as the :envvar:`LD_LIBRARY_PATH`.
        Update :envvar:`PATH` to locate :program:`ED.exe`.
        """
        if not self.isInstalled():
            return {}
        # library sub-packages
        libs = "STCoordinates IoSd IoAuger Ec Es Er".split()
        libPaths = []
        env = {}
        for l in libs:
            v = self.packageVersion(l)
            p = os.path.join(self.prefix, l, v)
            env["CDAS%sHOME" % l.upper()] = p
            libPaths.append(os.path.join(p, self.cmtSystem))
        env["LD_LIBRARY_PATH"] = ":".join(libPaths)
        env["PATH"] = os.path.join(self.prefix, "ED",
                                   self.packageVersion("ED"),
                                   self.cmtSystem)
        return env

    def isInstalled(self):
        """Determine if the cdas_ package is installed by looking for
        :program:`ED.exe`.
        """
        return os.path.exists(self.prefix) and \
            os.path.exists(os.path.join(self.prefix, 'ED')) and \
            os.path.exists(os.path.join(self.prefix, 'ED',
                                        self.packageVersion('ED'),
                                        self.cmtSystem, 'ED.exe'))

    def unpack(self):
        """Unpack into the install location.

        Unpack the cdas_ package into its final location. It has to be built
        there, since CMT_ does not install packages after building. The only
        activity of the *install* phase is to clean up temporary files.
        """
        Build.Package.unpack(self, self.sourceDirectory)
