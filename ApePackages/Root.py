"""
Build support for CERN's root_ package
--------------------------------------
"""
import os.path
import shutil

from ApeTools import Config, Build


class Root(Build.Package):

    builder_default = "cmake"

    def __init__(self, name, builder):
        """Defines extra :attr:`enable` and :attr:`disable` list attributes
        and reads their values from the configuration. These attributes are
        used to determine the list of ``--enable-*`` and ``--disable-*``
        arguments to :command:`./configure`. This implementation keeps the
        option lists in the configuration files shorter.
        """
        Build.Package.__init__(self, name, builder)
        self.setAttributes("enable disable configureArgs", Config.getlist)

        self.configureArgs += ["-D%s=OFF" % p for p in self.disable] + \
                              ["-D%s=ON" % p for p in self.enable]


    def configure(self, logFile, env):
        """Add in a custom python if requested.
        """
        if "PYTHONHOME" in env and not "python" in self.disable:
            self.configureArgs += ["-DPYTHON_EXECUTABLE=%s" % env["PYTHON"],
                                   "-DPYTHON_LIBRARY=%s" % env["PYTHON_LIBRARY"],
                                   "-DPYTHON_INCLUDE_DIR=%s" % env["PYTHON_INCLUDE_DIR"],
                                  ]
        Build.Package.configure(self, logFile, env)


    def build(self, env):
        """Remove the :envvar:`ROOTSYS` environment variable before
        before calling :meth:`ApeTools.Build.Package.build` to build root_.
        """
        try:
            del env["ROOTSYS"]
        except KeyError:
            pass
        Build.Package.build(self, env)

    def unpack(self):
        """Unpack root_. The top level directory in the tar-ball is always
        called ``root``. We rename it to ``root-``\ *version*.
        """
        Build.Package.unpack(self)
        if Config.getboolean("ape", "dryRun"):
            return
        # root switched to versioned dirs in tarballs since version 6
        oldstyle_dir = os.path.join(os.path.dirname(self.sourceDirectory), "root")
        if os.path.exists(oldstyle_dir):
            if os.path.exists(self.sourceDirectory):
                shutil.rmtree(self.sourceDirectory)
            shutil.move(oldstyle_dir, self.sourceDirectory)
