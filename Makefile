# Makefile to build the ape distribution
#
VERSION = $(shell ./ApeTools/Version.py)
DIRNAME = ape-$(VERSION)
TARNAME = ape-$(VERSION).tar.bz2

SOURCES = $(wildcard ApePackages/*.py) $(wildcard ApeTools/*.py) \
          $(wildcard config/*) Documentation Patches \
          $(wildcard Tests/*.py) Tests/run Tests/test.rc \
          $(wildcard ape.rc*) ape ape.py

SOURCES_REMOVE = Documentation/_build

all:
	@echo Specify target '"doc"' or '"dist"' or '"test"'.

doc:
	make -C Documentation install

export COPYFILE_DISABLE=1

dist:
	mkdir $(DIRNAME)
	tar cf - $(SOURCES) | (cd $(DIRNAME); tar xvf -)
	rm -rf $(DIRNAME)/$(SOURCES_REMOVE)
	tar cjvf $(TARNAME) $(DIRNAME)
	rm -rf $(DIRNAME)

test:
	Tests/run
