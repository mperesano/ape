# Central ape configuration file
#
# The script looks for in the following 5 locations (in order of priority):
# - file named by --rc=... option
# - ~/.aperc
# - $APERC
# - this file
#
# Python substitions of values inside the same section or from DEFAULT
#
# Author: Lukas Nellen
# Date: 10/May/2009

###############################################################################
# Default section - values set here appear as if set in other sections
# The 'base' variable gets set to the current directory at invocation.
# 'apedir' is set to the directory of the ape source. It should not be changed.
# 'home' is set to the user's home directory
[DEFAULT]
# directory locations for packages
#base = current directory; set automatically
build = %(base)s/build

# for gccselect
gccsuffix =

###############################################################################
# section for installation - can override
[ape]
# directory for logfiles
logs = %(base)s/logs

# directory to hold package archives
distfiles = %(apedir)s/distfiles

# preferred mirrors in order, can be empty to follow order of the [mirror ...] sections
mirrors =

# jobs to run in parallel
jobs = 1

# keep build directories after successful install
keep = False

# for testing in set-ups without commandline parsing, e.g., unit-tests.
verbose = False
dryRun = False

# pre dependencies, indirect to make it easier to set platform specific values
# and keep them changeable by the user
preDependencies = %(platformPreDependencies)s
platformPreDependencies =

# password file for downloads
pwFile =

###############################################################################
# ape internal setup - change at your own risk
[ape internal]
# patches - override at your own risk
patches = %(apedir)s/Patches

# file with sha1 checksums
sha1File = %(apedir)s/config/SHA1

###############################################################################
# ape empty section. Don't put anything here so we can see the defaults.
[ape empty]

###############################################################################
# Default package configuration
#
[package]
dependencies =
weakDependencies =
tarballs =
extraFiles =
patches =
alwaysInstall = False
alwaysKeep = False
isHidden = False
patchCmd = patch
patchArgs = -p1 -i
environment =
buildDirectory = %(sourcedirectory)s
dataDirectory =
parallelBuild = True
parallelAuto = False
infoVariables = builder name prefix buildDirectory sourceDirectory dataDirectory version tarballs extraFiles dependencies weakDependencies isHidden configureCmd configureArgs parallelBuild parallelAuto _env
# most packages need one or both of these
env.path = %(prefix)s/bin
env.ld_library_path = %(prefix)s/lib
env.pkg_config_path = %(prefix)s/lib/pkgconfig

[builder autoconf]
configureCmd = ./configure
configureArgsDef = --prefix=%(prefix)s
configureArgs = %(configureArgsDef)s
makeCmd = make
makeArgs =
installCmd = make
installArgs = install

[builder cmake]
configureCmd = cmake
# use system default generator,
# set to -GNinja to use ninja to build cmake projects
cmakeGenerator =
cmakePrefix = CMAKE_INSTALL_PREFIX
cmakeDirectory = %(sourceDirectory)s
configureArgsDef =  %(cmakeGenerator)s %(cmakeDirectory)s -D%(cmakePrefix)s=%(prefix)s -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_STANDARD=11
cmakeOptions =
cmakePackageOptions =
configureArgs = %(configureArgsDef)s %(cmakePackageOptions)s %(cmakeOptions)s
makeCmd = cmake
makeArgs = --build %(buildDirectory)s --
installCmd = cmake
installArgs = --build %(buildDirectory)s -- install

###############################################################################
#
# Mirrors
# All section names are of the form [mirror tag]
#
[mirror]
mirrorUser = AugerPrime

[mirror mx]
location = I de Ciencias Nucleares, UNAM, Mexico City, Mexico
url = https://www.auger.unam.mx/raw/distribution

[mirror us]
location = Lehman College, New York, USA
url = http://margarat.lehman.edu/auger/apeDist

[mirror de]
location = Bergische Universitaet Wuppertal, Germany
url = https://auger.uni-wuppertal.de/ApeDist
