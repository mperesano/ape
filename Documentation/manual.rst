.. _user-manual:

``Ape`` user guide
==================

.. note:: **update notice**

   The conventions for package names used in ``ape`` got changed to all
   lower-case. If you have used and configured ``ape`` for older versions, you
   might have to change the package names used in your configuration files.

Getting started
---------------

Dowloading ``ape``
^^^^^^^^^^^^^^^^^^

To download ``ape``, visit the `ape trac pages`_.

You can always get the current development version of ``ape`` from the
repository. The instructions for downloading are on the `ape trac pages`_.

.. _prerequisites:

Prerequisites for using ``ape``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To be able to use ``ape``, you have to have some packages installed on your
system. You need python_ and a C++-compiler as well as some development
libraries not yet provided by ``ape``. Please consult the `ape trac pages`_
for more information.

The required packages are readily available as part of all linux distributions.
For the Mac, you have to install XCode_. It comes on DVD with OS X.

Please consult the documentation for :ref:`mysql-support` for considerations
about MySQL_.

.. _python: http://www.python.org/

.. _XCode: http://developer.apple.com/TOOLS/Xcode/

.. _MySQL: http://www.mysql.com/

.. _simple-use:

Simple use
^^^^^^^^^^

It is possible to use ``ape`` with an empty configuration file, without
command line options. In this case,
``ape`` will install the packages into the directory ape resides in by setting
the *base* there.

.. code-block:: sh

   # Create empty ape config - not the recommended way of using ape!
   touch $HOME/.aperc

To download and install the current offline_ software and all
packages required to built it, invoke:

.. code-block:: sh

  ape install offline

If you only want the external packages required to build the offline_ software,
call:

.. _offline: https://www.auger.unam.mx/AugerWiki/OfflineSoftware


.. code-block:: sh

  ape install externals


.. note::
   Depending on the setting of your :envvar:`PATH`, you might have to
   specify ``./ape`` to invoke ``ape``.

To install to a different location, you can set the *base* variable either
by specifying the option :option:`-b`\ ``path`` or by providing a simple
configuration file. The latter is the recommended way. For a personal
installation, create :file:`~/.aperc`. If you prepare a site-wide installation,
you should provide :file:`ape.rc` in a central location and set the
:envvar:`APERC` environment variable. The way to set environment variables
at session start is system dependent. Please consult with your system
administrator about the means provided to configure the environment for all
users or for certain groups.

.. _simple-config-example:

A typical configuration file will be very short. Here is a simple example:

.. code-block:: ini

   # A simple ~/.aperc
   [DEFAULT]
   base = %(home)s/auger/software/ApeInstalled

   [ape]
   jobs = 2
   mirrors = mx us

The variables set here are:

  *base*
    The path to install all packages to. The hierarchy below is determined
    by the configuration of the individual packages installed. The example
    sets it relative to the user's :envvar:`HOME` environment variable.

    It is generally recommended to provide at least this variable in your
    configuration file to decouple the installation area from the ``ape`` code
    area.

    **NB:** The syntax used for variable substitution is Python's string
    interpolation syntax: ``%(var)s``. Don't forget the trailing ``s``.
    It is required and will not appear in the result.

  *jobs*
    The number of jobs to use for those packages which support parallel
    builds. The default value is 1. Set this to the number of cores in
    the machine your running ``ape`` on.

  *mirrors*
    The preferred mirror list. The example selects the Mexican mirror ahead of
    the US mirror, followed by the remaining mirrors known to ape. Setting
    this variable to a reasonable value can reduce the time required for
    downloading the packages significantly. Use ``ape mirrors`` to get the list
    of mirrors.

On a cluster with a shared home area, you might want to build on a local disk,
for example in the ``\tmp`` area. Your configuration file for compiling on a
cluster with multi-core servers might be:

.. code-block:: ini

   # A simple ~/.aperc
   [DEFAULT]
   base = %(home)s/auger/software/ApeInstalled
   build = /tmp/ape-build-%(user)s

   [ape]
   jobs = 10
   mirrors = mx us

Setting up your environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The sub-commands **sh** and **csh** dump shell instructions to set the
environment variables needed to use all or some of the packages installed
by ``ape``.

It is possible to install several independent packages with ``ape``. They can
require different, sometimes incompatible, configurations of the environment.
You should specify the name of the target package(s) as argument(s) to the
**sh** and **csh** subcommands of ``ape``.
For example, to set up the environment for the use of the Auger offline_
package, specify:

.. code-block:: sh

   # sh/bash/ksh
   eval `ape sh offline`

   # {t}csh
   eval `ape csh offline`

Dependencies are tracked. If a package depends on other packages, the
environment is also set up for the dependencies. Typical Auger users will
use the configuration for offline_, as shown in the example. Developers
should use :obj:`Externals` as the target for their environment. You can also
set up a customized environment for running the adst_ package if you use
``ape`` to install it.

.. _adst: http://augerobserver.fzk.de/doku.php?id=adstsoftware

You can try to set up the environment for all installed packages by no providing
the name of a target package.

.. code-block:: sh

   # sh/bash/ksh
   eval `ape sh`

   # {t}csh
   eval `ape csh`

.. note::

   This form of invoking the **sh** and **csh** sub-commands is more fragile than
   targeting a specific package. It will probably get removed in a future
   release of ``ape``.


More information can be found in the section :ref:`configuration-files`. You can
specify more than one package name as an argument to **sh** and **csh**.


.. _advanced-use:

Details of the ``ape`` command
------------------------------

The syntax of the ``ape`` command is

.. code-block:: text

  ape [options...] verb [packages...]

The ``verb`` indicates the action to be taken.

Verbs for installation
^^^^^^^^^^^^^^^^^^^^^^

**install**
  Installs the selected package(s) and their dependencies. If the option
  :option:`--jobs` is given, building will be done in parallel when possible.

**fetch**
  Download the source archives for the packages listed on the command line.
  Unless the option :option:`--no-dependencies` is given, this will also
  download the source archives of all dependencies.

**unpack**
  Unpack the source archives for the packages listed on the command line.
  Unless the option :option:`--no-dependencies` is given, this will also
  unpack the source archives of all dependencies.

**clean**
  Clean the build area for all installed packages. This way, one can remove the
  build directories which preserved due to the :option:`--keep` option or due to
  some configuration settings.

  .. note:: There is no way to force the removal of a build directory which
     is preserved due to the conditions listed
     in :meth:`ApeTools.Build.Package.clean`.


Verbs to provide information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**installed**
  List the installed packages.

**mirrors**
  List the mirror sites known to ``ape``. This list will be ordered as specified
  in the selection of preferred mirrors, e.g., by using the :option:`--mirrors`
  option.

**packages**
  List all packages known to ``ape``. The dependencies are also liste, unless
  suppressed using the option :option:`--no-dependencies`. If running in
  :option:`--verbose` mode, the version number are also listed.

**package-config**
  Dump the configuration data for the listed packages. If no package is listed,
  dump the information for all known packages.

**ape-config**
  Dump the global configuration of ``ape``.

**dump-config**
  Write the information provided by **package-config** and **ape-config** into
  a log-file name ``ape-dump.apelog`` in the *logs* directory.

.. _verbs-configure:

Verbs to propagate information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**sh**
  Dump a set of commands to configure the user's environment as specified by
  the listed packages. If no package name(s) given, produce an environment for
  the use of all installed packages.This verb provides commands for a shell of
  the *Bourne-Shell* family of shells.

  The commands emitted for path-like variables, e.g., :envvar:`PATH` or
  :envvar:`LD_LIBRARY_PATH` take into account if the variables exist already
  and emit the correct code for immediate use. No conditional shell code
  is produced.

**csh**
  Dump a set of commands to configure the user's environment as specified by
  the listed packages. If no package name(s) given, produce an environment for
  the use of all installed packages.This verb provides commands for a shell of
  the *C-Shell* family of shells.

  The commands emitted for path-like variables, e.g., :envvar:`PATH` or
  :envvar:`LD_LIBRARY_PATH` take into account if the variables exist already
  and emit the correct code for immediate use. No conditional shell code
  is produced.


Options
^^^^^^^

Options can be used to modify details of the action or output when running
``ape``.

.. option:: --base=<path>, -b <path>

   Specify the base of the tree to install the packages into. It is preferred,
   though, to set it in a :ref:`configuration file <configuration-files>`.

   By default, ``ape`` installs into the current directory.

.. option:: --jobs=<jobs>, -j <jobs>

   Set the number of *jobs* for building in parallel. Some packages cannot be
   built in parallel. This option will be ignored in that case.

.. option:: --verbose, -v

   Provide more information.

.. option:: --dry-run, -n

   Do not actually download, unpack, build, or install anything. This provides
   the user with a way to see what ``ape`` intends to do.

.. option:: --keep, -k

   Keep the directories where the packages where build. In general, ``ape`` will
   remove the build area, since it is no longer needed after a successful
   install.

.. option:: --force, -f

   Force ``ape`` to perform the selection on the packages listed on the command
   line. This can be used, e.g., to re-install a package.

.. option:: --force-all

   Like :option:`--force`, but applies also to all dependencies, not just the
   packages listed on the command line.

.. option:: --no-dependencies, -x

   Ignore the dependencies of a package. This option can be useful for fetching
   exactly the source files for selected packages, or to reduce the output
   when listing the packages known to ``ape``. It should not be used during
   an installation, or you might end up with an inconsistent set-up.

.. option:: --mirrors, -m

   Provide a colon-separated list of mirror sites which should be tried first
   when fetching package sources.

.. option:: --debug, -d

   Provide additional debugging information.

.. option:: --rc=<file>

   Specifies an additional configuration file to be read.

   .. note:: This should be used only for debugging, in scripts or for one-off
      installations. Otherwise, the results might be hard to reproduce. This is
      especially true if you intend to use the :ref:`verbs-configure` to set
      up your environment.

.. option:: --pw-file=<file>

   Specifies the file which contains user/password pairs for fetching externals.
   For more details, including the format of the file, see the section
   on `Using a password file`_.

   .. note:: This suppresses interactive prompting for a password.

.. option:: --option=<assignment>, -o <assignment>

   Set a configuration variable directly from the command line. For example, to
   provide a list of preferred mirrors, you could use any of the following:

   - Provide the information as an option:

     .. code-block:: sh

       ape --mirrors=mx:us fetch externals

   - Set the *ape.mirrors* configuration variable on the command line:

     .. code-block:: sh

       ape --option='ape.mirrors = mx us' fetch externals

   - Provide the information in a ``.rc`` file:

     .. code-block:: ini

        # part of ~/.aperc
        [ape]
        mirrors = mx us

     And run ``ape`` without any options:

     .. code-block:: sh

        ape fetch externals


Troubleshooting
---------------

The output of all commands executed while building the packages is collected in
a single directory *logs*. Unless set explicitly, this directory is below the
*base* of the installation. There is a separate log-file for each stage of
building and installing a package. All log-files have the extension ``.apelog``.
In case of failure, the final error message will also be logged in a file named
``ape-failure.apelog``. The information in this file indicates the stage where
``ape`` encountered a problem and an indication of what failed. The first two
lines of a log-file indicate the host where the command was executed and the
command being run:

.. code-block:: text

   ape >>> on host firefly.lan
   ape >>> make

This extra information can help to reconstruct what went wrong. Knowing the
host can be important to diagnose spurious problems in a heterogeneous cluster.

If you cannot solve the problem, consult with the experts on the user and
developer mailing lists. You might get asked to provide some or all of the files
in the *logs* directory.

If you think you found a bug in ``ape``, please report it at the
`ape trac pages`_.


.. _configuration-files:

The ``ape`` configuration files
-------------------------------

Most data needed by ``ape`` is provided in configuration files and not in
the code itself. In this section, we deal with the customization of ``ape``
for individual and site set-ups. Advanced issues, including teaching ``ape``
about new packages, get discussed in the section :ref:`extending-ape`. The
section :ref:`simple-use` contains a :ref:`simple configuration example
<simple-config-example>`.

Format of configuration files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The configuration files are in INI-format, which can be handled by Python's
:mod:`ConfigParser` modules. This format looks like:

.. code-block:: ini

   # a comment
   [section]
   var = value
   another.var = yet another value

   [section 2]
   # var has a different value in this section
   var = value2
   # the value of var.extendend is "value2 and more".
   var.extended = %(var)s and more

A variable is identified by the section and a name within a section. By
convention, ``ape`` does not allow ``.`` in section names, only in variable
names. Values of variables can be substituted into other variables, using the
Pythonic notation ``%(``\ *var*\ ``)s``. This is used extensively by ``ape``
to build up path names.

Comments are allowed and can be introduced with ``#`` or ``;``.

Variable types
^^^^^^^^^^^^^^

Different variable types are supported in configuration files:

**string**
  An arbitrary string value. Leading white-space is stripped.

**integer**
  A string which can be converted to an integer.

**boolean**
  A string which can be converted to a boolean. The values ``on``, ``yes``,
  ``1``, and ``true`` indicate a ``True`` value. The values ``off``, ``no``,
  ``0``, and ``false`` indicate a ``False`` value. The conversion is
  case-insensitive.

**list of strings**
  A white-space separated list of strings. The strings cannot contain any
  white-space.

  To make customization simpler, a variable *var* has the following variables
  associated:

  *var.delete*
    A list of items to remove from *var*

  *var.prepend*
    A list of items to prepend to *var*

  *var.append*
    A list of items to append to *var*

  .. note:: These customization variables are reserved for the user or local
     administrator and cannot be used in the configuration files supplied with
     ``ape``.


Section names
^^^^^^^^^^^^^

The sub-components of ``ape`` look in different sections for configuration
variables. Some sections provide default values for other sections.

``[DEFAULT]``
  This is the global default section. Variables set in this section will appear
  visible in **all** other sections. A few variables are provided automatically
  by ``ape``: *home*, *user*, *apeDir*, *base*. The build directory *build* is
  also set in this section. Do not add variables to this section unless you are
  200% sure of what you are doing.

  Most likely, you are going to change *base*. On clusters, where *base* is
  on a remote file-system, you might set *build* to a path on a local disk.

  .. code-block:: ini

     [DEFAULT]
     base = /shared/ApeInstalled
     build = /local/build/path


``[ape]``
  This section contains the global variables for ``ape``. The variables you are
  likely to set are *jobs* and *mirrors*.

  .. code-block:: ini

     [ape]
     # list of tags for mirrors in order of preference
     mirrors = mx us es
     # number of processed for parallel builds
     jobs = 4

  Additionally, you can add variables *env.var* to this section to set
  environment variables:

  .. code-block:: ini

     [ape]
     # force packages which look at the environment variables CC and CXX
     # to use the gnu compilers.
     env.cc = gcc
     env.cxx = g++


``[ape internal]``
  **Do not touch.**

``[package]``
  Default values of most variables used to configure the building of packages.

``[package`` **name**\ ``]``
  Variables to configure the package called **name**. ``Ape`` determines the
  set of known packages by looking for all sections who's names start with
  ``package`` followed by a package name.

``[mirror]``
  Default values for setting up data mirror sites.

  .. code-block:: ini

     [mirror]
     user = cuyam

``[mirror`` **tag**\ ``]``
  Defines the variables for a mirror identified by **tag**. The configuration
  for the *us* mirror is:

  .. code-block:: ini

     [mirror us]
     location = Northeastern University, Boston, USA
     url = http://129.10.132.228/apeDist

  A site might want to set up a local mirror and make it the preferred mirror
  to use.

Using a password file
^^^^^^^^^^^^^^^^^^^^^

.. note:: Storing a password in clear text in a file is in general a security
   risk and should be avoided. This feature is primarily intended to support
   frequent, automated installations for test purposes.

You can store the download password(s) in a file, which can be specified either
in the configuration file or using a the command line option :option:`--pw-file`.
To specify the file in your configuration file, you have to set the ``pwFile``
variable it in the ``[ape]`` section of the file:

.. code-block:: ini

   [ape]
    # other options ...
    # password file is .apepw in my home directory
    pwFile = %(home)s/.apepw

The file contains one or more lines with user/password pairs::

  user 12345
  luser qwerty
  noone password

The file has to be protected to be accessible only by the user, without execute
permissions i.e., file mode 0400 or 0600. The presence of a password file will
suppress the interactive prompt.

Customizing packages
--------------------

A few packages have variables which can be customized.

Disabling the installation of a package
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You might want to avoid the installation of a package by ``ape``, e.g.,
because you prefer to use a version of the package provided by your system.

.. note::

   Don't do this unless you are know what you are doing and you are absolutely convinced
   that this is the right thing to do.

You hide a package from the installation by setting its attribute :attr:`isHidden`:

.. code-block:: ini

   [package excludeme]
   isHidden = True

The package will be excluded from the list of known packages when no package is requested explicitly and
from all dependencies of other packages
Operations line installation and the emission of the environment will ignore the package.
Some some operations of the package a still possible, though. For example, information about the package
can still be interrogated, which means :samp:`ape package-config excludeme` will always provide a dump
of the important attributes of the package.

.. note::

   It is **strongly** recommended that you remove your existing installation and re-install if you change
   this flag for *any* package.

Disabling by removing a package from dependencies
+++++++++++++++++++++++++++++++++++++++++++++++++

Trying to prevent the installation of a package by removing it from the :attr:`dependencies` and
:attr:`weakDependencies` of *all* other packages is possible, but not reliable. You can miss dependencies
and dependencies in the default configuration are likely to change in the future.

Disabling by removing sources and environment
+++++++++++++++++++++++++++++++++++++++++++++

An alternative is to turn a package into a *dummy* by clearing the list of
sources and environment variables for that package:

 .. code-block:: ini

    [package excludeme]
    tarballs =
    extraFiles =
    environment =

This declares that there are no files and no environment variables for the
package. As a consequence, there is nothing to do for this package. Its
dependencies are still processed, unless you remove those, too.

Customizing aires_
^^^^^^^^^^^^^^^^^^

You can select the Fortran compiler used for building aires_ by setting the
*fc* variable.

.. code-block:: ini

   [package aires]
   # use g77 instead of gfortran, needed if you have the 3.x GNU compilers
   fc = g77

.. _aires: http://www.fisica.unlp.edu.ar/auger/aires/


Customizing boost_
^^^^^^^^^^^^^^^^^^

You might want to change the individual libraries built as part of your
installation of boost_. You can do this by adding and removing options from
the *configureArgs* variable.

.. code-block:: ini

   [package boost]
   configureArgs.delete = --without-python
   configureArgs.append = --without-iostreams

.. _boost: http://www.boost.org/


Customizing root_
^^^^^^^^^^^^^^^^^

The configuration script of root_ takes a large number
of options to select which sub-systems to include and exclude in a build.
The options, without the strings ``--enable-`` or ``--disable-``, are listed in
the string lists *enable* and *disable*.

Assuming you would like to disable Python and include Ruby (if possible), you
specify:

.. code-block:: ini

   [package root]
   disable.delete = ruby
   disable.append = python

.. _root: http://root.cern.ch/


Customizing ``externals``
^^^^^^^^^^^^^^^^^^^^^^^^^

You can customize the list of dependencies of the ``externals`` package. By
default, this package depends on all required and optional packages the
Auger Offline software depends on. Assuming you would like to exclude aires_,
you specify:

.. code-block:: ini

   [package externals]
   dependencies.delete = aires


Selecting a non-standard compiler
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``gccselect`` package
+++++++++++++++++++++++++

The ``gccselect`` package allows the user to override the compilers used. It
provides a ``bin`` directory with the standard names of compilers symbolically
linked to names with version suffixes and pre-pending this to the :envvar:`PATH`
environment variable.

To use the package, the user has to insert the following lines into the personal
ape configuration in :file:`~/.aperc` or equivalent locations, as
documented earlier:

.. code-block:: ini

   [DEFAULT]
   # suffix to identify versioned compilers
   gccsuffix = -4.8

   [ape]
   # prepend the next package to all dependencies
   platformPreDependencies = gccselect

This mechanism was originally added to be able to use the gcc compiler suite on
OS X.
