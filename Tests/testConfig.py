#! /usr/bin/env python

# Configure path to find modules to test
import sys
import os.path
if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(sys.path[0]))

apeDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

import unittest
import optparse
from ApeTools import Config


class testConfig(unittest.TestCase):
    def setUp(self):
        self.parser = optparse.OptionParser()
        Config.addOptions(self.parser)
        Config.init()

    def testSections(self):
        self.assert_(len(Config.sections()) > 1)

    def testInternal(self):
        self.assertEqual(Config.get("ape internal", "apedir"), apeDir)
        self.assertEqual(Config.get("ape internal", "patches"),
                         os.path.join(apeDir, "Patches"))

    def testSetOptionShort(self):
        options = self.parser.parse_args(["-o", "ape.a=alfa",
                                          "-oape.b=beta"])[0]
        Config.init(options)
        self.assertEqual(Config.get("ape", "a"), "alfa")
        self.assertEqual(Config.get("ape", "b"), "beta")

    def testSetOptionLong(self):
        options = self.parser.parse_args(["--option=ape.a=alfa",
                                          "--option=ape.b=beta"])[0]
        Config.init(options)
        self.assertEqual(Config.get("ape", "a"), "alfa")
        self.assertEqual(Config.get("ape", "b"), "beta")

    def testSetOptionMixed(self):
        options = self.parser.parse_args(["--option=ape.a=alfa", "-o",
                                          "ape.b=beta"])[0]
        Config.init(options)
        self.assertEqual(Config.get("ape", "a"), "alfa")
        self.assertEqual(Config.get("ape", "b"), "beta")

    def testEnvironment(self):
        options = self.parser.parse_args(["--option=ape.env.ape_test=monkey"])[0]
        Config.init(options)
        self.assertEqual(os.environ["APE_TEST"], "monkey")

    def testList(self):
        mirrors = Config.getlist("ape", "mirrors")
        self.assert_("mx" in mirrors)

    def testListOperations(self):
        Config.config.set("ape", "testList", "a b c d")
        self.assertEqual(Config.getlist("ape", "testList"),
                         ["a", "b", "c", "d"])
        Config.config.set("ape", "testList.delete", "a c")
        self.assertEqual(Config.getlist("ape", "testList"),
                         ["b", "d"])
        Config.config.set("ape", "testList.append", "y z")
        self.assertEqual(Config.getlist("ape", "testList"),
                         ["b", "d", "y", "z"])
        Config.config.set("ape", "testList.prepend", "1")
        self.assertEqual(Config.getlist("ape", "testList"),
                         ["1", "b", "d", "y", "z"])

        Config.config.set("ape", "testShList", 'C=d E="a b"')
        self.assertEqual(Config.getlist("ape", "testShList"),
                         ['C=d', 'E=a b'])
        Config.config.set("ape", "testShList.delete", 'E="a b"')
        self.assertEqual(Config.getlist("ape", "testShList"),
                         ['C=d'])
        Config.config.set("ape", "testShList.append", 'P="v w"')
        self.assertEqual(Config.getlist("ape", "testShList"),
                         ['C=d', 'P=v w'])
        Config.config.set("ape", "testShList.prepend", r'F="a \"b\""')
        self.assertEqual(Config.getlist("ape", "testShList"),
                         [r'F=a "b"', 'C=d', 'P=v w'])

    def testFallback(self):
        Config.config.add_section("s1")
        Config.config.set("s1", "o1", "v1")
        Config.config.set("s1", "o2", "v2")
        Config.config.set("s1", "o3", "v3")
        Config.config.add_section("s2")
        Config.config.set("s2", "o2", "different")
        self.assertEqual(Config.get("s2", "o1", ["s1"]), "v1")
        self.assertEqual(Config.get("s2", "o2", ["s1"]), "different")
        self.assertEqual(Config.get("s2", "o3", ["s1"]), "v3")

        Config.config.set("s2", "o1", "from s2")
        Config.config.add_section("s3")
        Config.config.set("s3", "o2", "changed")
        self.assertEqual(Config.get("s3", "o1", ["s2", "s1"]), "from s2")
        self.assertEqual(Config.get("s3", "o2", ["s2", "s1"]), "changed")
        self.assertEqual(Config.get("s2", "o3", ["s1"]), "v3")

if __name__ == '__main__':
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
